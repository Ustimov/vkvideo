//
//  UAVVideoViewController.h
//  vk-video
//
//  Created by Artem Ustimov on 3/20/17.
//  Copyright © 2017 Artem Ustimov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UAVVideo.h"

@interface UAVVideoViewController : UIViewController

@property (nonatomic, strong) UAVVideo * video;

@end
