//
//  UAVVideoViewController.m
//  vk-video
//
//  Created by Artem Ustimov on 3/20/17.
//  Copyright © 2017 Artem Ustimov. All rights reserved.
//

#import "UAVVideoViewController.h"

@import WebKit;

@interface UAVVideoViewController ()

@end

@implementation UAVVideoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = self.video.title;
    
    WKWebView * webView = [[WKWebView alloc] initWithFrame:self.view.frame];
    [webView loadRequest:[NSURLRequest requestWithURL:[[NSURL alloc] initWithString:self.video.videoUrl]]];
    
    [self.view addSubview:webView];
}

@end
