//
//  UAVWebViewController.m
//  vk-video
//
//  Created by Artem Ustimov on 3/20/17.
//  Copyright © 2017 Artem Ustimov. All rights reserved.
//

#import "UAVWebViewController.h"
#import "UAVSearchViewController.h"
#include "UAVNetworking.h"

@import WebKit;

@interface UAVWebViewController () <WKNavigationDelegate>

@end

@implementation UAVWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString * formatString = @"https://oauth.vk.com/authorize?client_id=%i&display=page&redirect_uri=https://oauth.vk.com/blank.html&scope=video&response_type=token&v=5.52";
    int appId = 0;
    NSString * urlString = [NSString stringWithFormat:formatString, appId];
    
    CGRect webViewRect = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y + 20, self.view.frame.size.width, self.view.frame.size.height);
    WKWebView * webView = [[WKWebView alloc] initWithFrame:webViewRect];
    webView.navigationDelegate = self;
    [webView loadRequest:[NSURLRequest requestWithURL:[[NSURL alloc] initWithString:urlString]]];

    [self.view addSubview:webView];
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    NSString * accessToken = [self tryToGetAccessTokenFromFragment:webView.URL.fragment];
    if (accessToken) {
        [[UAVNetworking sharedInstance] setAccessToken:accessToken];
        UAVSearchViewController * searchViewController = [[UAVSearchViewController alloc] init];
        UINavigationController * navigationController = [[UINavigationController alloc] initWithRootViewController:searchViewController];
        [self presentViewController:navigationController animated:YES completion:nil];
    }
}

- (NSString *)tryToGetAccessTokenFromFragment:(NSString *)fragment {
    NSString * accessToken = nil;
    NSArray * params = [[[fragment componentsSeparatedByString:@"&"] firstObject] componentsSeparatedByString:@"="];
    if ([params count] > 0 && [params[0] isEqualToString:@"access_token"]) {
        accessToken = params[1];
    }
    return accessToken;
}

@end
