//
//  UAVAppDelegate.h
//  vk-video
//
//  Created by Artem Ustimov on 3/20/17.
//  Copyright © 2017 Artem Ustimov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UAVAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

