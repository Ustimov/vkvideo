//
//  UAVAppDelegate.m
//  vk-video
//
//  Created by Artem Ustimov on 3/20/17.
//  Copyright © 2017 Artem Ustimov. All rights reserved.
//

#import "UAVAppDelegate.h"
#import "UAVWebViewController.h"

@interface UAVAppDelegate ()

@end

@implementation UAVAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    UAVWebViewController * webViewController = [[UAVWebViewController alloc] init];

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    self.window.rootViewController = webViewController;

    [self.window makeKeyAndVisible];

    return YES;
}

@end
