//
//  UAVVideo.h
//  vk-video
//
//  Created by Artem Ustimov on 3/20/17.
//  Copyright © 2017 Artem Ustimov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UAVVideo : NSObject

@property (nonatomic, strong) NSString * title;

@property (nonatomic) int duration;

@property (nonatomic, strong) NSString * photoUrl;

@property (nonatomic, strong) NSString * videoUrl;

@end
