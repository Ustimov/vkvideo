//
//  UAVNetworking.m
//  vk-video
//
//  Created by Artem Ustimov on 3/20/17.
//  Copyright © 2017 Artem Ustimov. All rights reserved.
//

#import "UAVNetworking.h"
#import "UAVVideo.h"

@interface UAVNetworking ()

@property (nonatomic, strong) NSURL * baseUrl;

@end

@implementation UAVNetworking

+ (instancetype)sharedInstance {
    static UAVNetworking * sharedInstance = nil;
    if (!sharedInstance) {
        sharedInstance = [[UAVNetworking alloc] init];
    }
    return sharedInstance;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _baseUrl = [[NSURL alloc] initWithString:@"https://api.vk.com/method/"];
    }
    return self;
}

- (void)loadVideoForQuery:(NSString *)query withOffset:(int)offset handler:(void(^)(NSArray * videos))videoLoadedHandler {
    NSMutableString * urlString = [NSMutableString stringWithString:@"video.search?v=5.52&sort=2&hd=0&count=40&filters=mp4"];
    [urlString appendFormat:@"&access_token=%@", self.accessToken];
    [urlString appendFormat:@"&q=%@", query];
    [urlString appendFormat:@"&offset=%i", offset];
    
    NSString * escapedUrl = [urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:escapedUrl relativeToURL:self.baseUrl]];

    NSURLSession * session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request completionHandler:^(NSData * data, NSURLResponse * response, NSError * error) {
        if (data) {
            videoLoadedHandler([self processData:data]);
        }
    }] resume];
}

- (NSArray *)processData:(NSData *)data {
    NSDictionary * dictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    NSMutableArray * videos = [[NSMutableArray alloc] init];
    for (NSDictionary * item in dictionary[@"response"][@"items"]) {
        UAVVideo * video = [[UAVVideo alloc] init];
        video.title = item[@"title"];
        video.duration = [item[@"duration"] integerValue];
        video.photoUrl = item[@"photo_130"];
        video.videoUrl = item[@"player"];
        [videos addObject:video];
    }
    return videos;
}

- (void)loadImageFrom:(NSString *)urlString withHandler:(void(^)(UIImage *))imageLoadedHandler {
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    NSURLSession * session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request completionHandler:^(NSData * data, NSURLResponse * response, NSError * error) {
        if (data) {
            imageLoadedHandler([UIImage imageWithData:data]);
        }
    }] resume];
}

@end
