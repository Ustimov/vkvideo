//
//  UAVNetworking.h
//  vk-video
//
//  Created by Artem Ustimov on 3/20/17.
//  Copyright © 2017 Artem Ustimov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UAVNetworking : NSObject

@property (nonatomic, strong) NSString * accessToken;

+ (instancetype)sharedInstance;

- (void)loadVideoForQuery:(NSString *)query withOffset:(int)offset handler:(void(^)(NSArray * videos))videoLoadedHandler;

- (void)loadImageFrom:(NSString *)urlString withHandler:(void(^)(UIImage *))imageLoadedHandler;

@end
