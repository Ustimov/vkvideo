//
//  UAVSearchViewController.m
//  vk-video
//
//  Created by Artem Ustimov on 3/20/17.
//  Copyright © 2017 Artem Ustimov. All rights reserved.
//

#import "UAVSearchViewController.h"
#import "UAVNetworking.h"
#import "UAVVideo.h"
#import "UAVVideoViewController.h"
#import "UAVWebViewController.h"

@interface UAVSearchViewController () <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate>

@property (nonatomic, strong) NSMutableArray * videos;

@property (nonatomic, strong) UITableView * tableView;

@property (nonatomic, strong) NSString * query;

@end

@implementation UAVSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"VK Video";
    
    UIBarButtonItem * logoutButton = [[UIBarButtonItem alloc] initWithTitle:@"Выход" style:UIBarButtonItemStylePlain target:self action:@selector(logout)];
    self.navigationItem.rightBarButtonItem = logoutButton;
    
    self.videos = [[NSMutableArray alloc] init];
    
    self.tableView = [[UITableView alloc] initWithFrame:self.view.frame style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    UISearchBar * searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    searchBar.delegate = self;
    self.tableView.tableHeaderView = searchBar;

    [self.view addSubview:self.tableView];
}

- (void)logout {
    NSString * libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString * cookiesFolderPath = [libraryPath stringByAppendingString:@"/Cookies"];
    [[NSFileManager defaultManager] removeItemAtPath:cookiesFolderPath error:nil];
    
    UAVWebViewController * webViewController = [[UAVWebViewController alloc] init];
    [self presentViewController:webViewController animated:YES completion:nil];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    UIActivityIndicatorView * activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicatorView.center = self.view.center;
    [activityIndicatorView startAnimating];
    
    [self.view addSubview:activityIndicatorView];
    
    self.query = searchBar.text;
    [[UAVNetworking sharedInstance] loadVideoForQuery:self.query withOffset:0 handler:^(NSArray * videos) {
        [self.videos removeAllObjects];
        [self.videos addObjectsFromArray:videos];
        [self.tableView reloadData];
        [activityIndicatorView stopAnimating];
    }];
    [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.videos count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
    }
    
    if ([self.videos count] - indexPath.row < 15) {
        [self loadNextVideo];
    }
    
    UAVVideo * video = self.videos[indexPath.row];
    
    int hours = video.duration / (60 * 60);
    int minutes = (video.duration / 60) % 60;
    int seconds = video.duration % 60;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%02i:%02i:%02i", hours, minutes, seconds];
    cell.textLabel.text = [NSString stringWithFormat:@"%@", video.title];
    cell.imageView.image = [UIImage imageNamed:@"loading.gif"];
    cell.tag = indexPath.row;
    
    [[UAVNetworking sharedInstance] loadImageFrom:video.photoUrl withHandler:^(UIImage * image) {
        if (cell.tag == indexPath.row) {
            cell.imageView.image = image;
            [cell layoutSubviews];
        }
    }];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UAVVideoViewController * videoViewController = [[UAVVideoViewController alloc] init];
    videoViewController.video = self.videos[indexPath.row];
    [self.navigationController pushViewController:videoViewController animated:YES];
}

- (void)loadNextVideo {
    [[UAVNetworking sharedInstance] loadVideoForQuery:self.query withOffset:[self.videos count] handler:^(NSArray * videos) {
        [self.videos addObjectsFromArray:videos];
        [self.tableView reloadData];
    }];
}

@end
