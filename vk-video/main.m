//
//  main.m
//  vk-video
//
//  Created by Artem Ustimov on 3/20/17.
//  Copyright © 2017 Artem Ustimov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UAVAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([UAVAppDelegate class]));
    }
}
